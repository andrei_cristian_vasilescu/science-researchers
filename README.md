Tema proiect: Science researchers application

Descriere: 
	Aplicatia poate fi utilizata pentru vizualizarea rapida a informatiilor
de interes din platforma Mendeley folosind diverse filtre. Dispune de posibilitatea
de a cauta documente de specialitate si grupuri de interes din care fac
parte diferite persoane cu experienta din domeniul cautat. Ajuta la interactiunea
rapida cu domeniul stiintific de interes.

Specificatii:
	- cautarea rapida a documentelor de interes
	- filtrarea documentelor si a lucrarilor
	- vizualizarea grupurilor din care fac parte persoane cu experienta in domeniu
	- interactiune cu alti uitlizatori din platforma Mendeley
	- postarea si adaugarea de noi documente si lucrari
	- vizualizarea informatiilor si documentelor ce apartin unor grupuri
	- cautare informatii din domeniul stiintific de interes
