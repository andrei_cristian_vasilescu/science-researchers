
const express = require('express');
const bodyParser = require('body-parser');
const Sequelize = require('sequelize');
let request = require("request");
var cors = require('cors')


let usernameAuth = "7737";
let passwordAuth = "vdu3cr6cLzXzgYnY";
let optionsAuth = {
    url: "https://api.mendeley.com/oauth/token",
    method: "POST",
    headers: {
        'Content-Type': 'application/x-www-form-urlencoded'
    },
    body: "grant_type=client_credentials&scope=all",
    auth: {
        user: usernameAuth,
        password: passwordAuth
    }
};

const sequelize = new Sequelize('science_researchers', 'root','root',{
    dialect : 'mysql',
    define : {
        timestamps:false
    }
});


//entities ******************

const User = sequelize.define('user', {
    first_name : {
        type : Sequelize.STRING,
        allowNull : false,
        validate : {
            len : [3,20]
        }
    },
    last_name : {
        type : Sequelize.STRING,
        allowNull : false,
        validate : {
            len : [3,20]
        }
    },
    username : {
        type : Sequelize.STRING,
        allowNull : false,
        validate : {
            len : [3,20]
        }
    },
    password : {
        type : Sequelize.STRING,
        allowNull : false,
        validate : {
            len : [3,20]
        }
    },
    birthday : {
        type : Sequelize.DATEONLY,
        allowNull: false
    },
    gender : {
        type : Sequelize.STRING(1),
        allowNull : false
    }
}, {
    underscored : true
});

const SubjectArea = sequelize.define('subject_area', {
    name : {
        type : Sequelize.STRING,
        allowNull : false,
        validate : {
            len : [3,100]
        }
    }
}, {
    underscored : true
});

const FavoriteDocument = sequelize.define('favorite_document', {
    document_id : {
        type : Sequelize.TEXT,
        allowNull : false
    },
    added_date : {
        type : Sequelize.DATEONLY,
        allowNull: false,
        defaultValue : Sequelize.NOW
    }
});

const FavoriteResearcher = sequelize.define('favorite_researcher', {
    researcher_id : {
        type : Sequelize.INTEGER,
        allowNull : false
    },
    added_date : {
        type : Sequelize.DATEONLY,
        allowNull: false,
        defaultValue : Sequelize.NOW
    }
});

const FavoriteSubjectArea = sequelize.define('favorite_subject_area', {});

User.hasMany(FavoriteSubjectArea);

SubjectArea.hasMany(FavoriteSubjectArea)

User.hasMany(FavoriteDocument);

User.hasMany(FavoriteResearcher);

// ************************


// utils

function getToken() {
    return new Promise(function (resolve, reject) {
        request(optionsAuth, function(err, response, body) {
            if(err) {
                reject(err);
            }
            let auth = JSON.parse(body);
            resolve(auth.access_token);
        })
    });
}

function mendeleyRequest(urlMendeley, token) {
    return new Promise(function(resolve, reject) {
        let auth = "Bearer " + token;
        request( {url: urlMendeley, method: "GET", headers: {"Authorization": auth}}, function(err, response, body) {
            if(err) {
                reject(err);
            }
            else {
                let result = JSON.parse(body);
                resolve(result);
            }
        });
    })
}

// ******

const app = express();
app.use(bodyParser.json());
app.use(cors())
app.use(express.static('../frontend/build'));

//create database
app.get('/createDatabase', (req, res) => {
    sequelize.sync({force : true})
        .then(() => {
            let tokenPromise = getToken();
            tokenPromise.then(function(result) {
                let mendeleyPromise = mendeleyRequest("https://api.mendeley.com/subject_areas",result);
                mendeleyPromise.then(function(resultMendeley) {
                    for(let i=0;i<resultMendeley.length;i++) {
                        let subjectArea = {};
                        subjectArea.name = resultMendeley[i].name;
                        SubjectArea.create(subjectArea).then( () => console.log(subjectArea.name + " was added")).catch((err)=>console.warn(err));
                    }
                    res.status(201).send('created tables');
                }).catch(function(err) {
                    console.warn(err);
                })

            }).catch(function(error) {
                console.warn(error);
            })

        })
        .catch((err) => {
            console.warn(err);
            res.status(500).send('some error...');
        });
});
//create database

//documents
app.get('/searchDocument', (req,res) => {
    let tokenPromise = getToken();
    tokenPromise.then(function(result) {
        let mendeleyPromise = mendeleyRequest("https://api.mendeley.com/search/catalog?query=" +  req.query.find + "&limit=15", result);
        mendeleyPromise.then(function(resultMendeley) {
            res.status(200).json(resultMendeley);
        }).catch(function(err) {
            console.warn(err);
            res.status(500).body("Some errors");
        })
    }).catch(function(error) {
        console.warn(error);
        res.status(500).body("Some errors");
    });
});

app.get('/searchProfile', (req,res) => {
    let tokenPromise = getToken();
    tokenPromise.then(function(result) {
        let mendeleyPromise = mendeleyRequest("https://api.mendeley.com/search/profiles?query=" +  req.query.find + "&limit=15", result);
        mendeleyPromise.then(function(resultMendeley) {
            res.status(200).json(resultMendeley);
        }).catch(function(err) {
            console.warn(err);
            res.status(500).body("Some errors");
        })
    }).catch(function(error) {
        console.warn(error);
        res.status(500).body("Some errors");
    });
});

app.get("/favoriteDocument/:id", (req,res) => {
    let tokenPromise = getToken();
    tokenPromise.then(function(result) {
        let mendeleyPromise = mendeleyRequest("https://api.mendeley.com/catalog/" +  req.params.id + "?view=all", result);
        mendeleyPromise.then(function(resultMendeley) {
            res.status(200).json(resultMendeley);
        }).catch(function(err) {
            console.warn(err);
            res.status(500).body("Some errors");
        })
    }).catch(function(error) {
        console.warn(error);
        res.status(500).body("Some errors");
    });
});

app.get("/favoriteDocument/user/:id", (req,res) => {
    FavoriteDocument.findAll({ where: {userId: req.params.id} })
    .then((documents) => {
        let response = {};
        if(documents.length !== 0) {
            response = [];
            for(let i=0;i<documents.length;i++) {
                response[i] = documents[i].dataValues;
            }
            res.status(200).json(response);
        }
        else {
            res.status(404).json(response);
        }
    })
    .catch((err) => res.status(500).send("Errors"));
});

app.post("/favoriteDocument", (req,res) => {
    let favoriteDucument = req.body;
    favoriteDucument.added_date = new Date();
    FavoriteDocument.create(favoriteDucument).then(() => res.status(201).send('Document was added'))
        .catch((err) => {
            console.warn(err)
            res.status(500).send('some errors')
        })
});

app.delete("/favoriteDocument/:id", (req,res) => {
    FavoriteDocument.findByPk(req.params.id)
        .then( (document) => {
            if(document) {
                document.destroy();
            }
            else {
                res.status(404).send("Favorite document not found");
            }
        })
        .then( () => {res.status(201).send("Favorite document deleted") })
            .catch((err) => {
                console.warn(err);
                res.status(500).send('some error');
            })

});
//documents

//users
app.post('/signup', (req, res) => {
    let user = req.body;
    console.log(user)
    User.create(user).then(() => res.status(201).send('Sign up succesfully'))
        .catch((err) => {
            console.warn(err)
            res.status(500).send('some error')
        })
});

app.post("/login", (req,res) => {
    let requestBody = req.body;

    User.findAll({ where: {username: requestBody.username, password: requestBody.password} })
        .then((user) => {
            let response = {};
            if(user.length !== 0) {
                res.status(200).json(user[0].dataValues);
            }
            else {
                response.message = "Authentication failed";
                response.user_id = -1;
                res.status(401).json(response);
            }
        })
        .catch((err) => res.status(500).send("Errors"));
});
//users


app.post('/favoriteSubjectArea', (req, res) => {
    let subjectArea = req.body;
    FavoriteSubjectArea.create(subjectArea).then(() => res.status(200).send('Added'))
        .catch((err) => {
            console.warn(err)
            res.status(500).send('some error')
        })
});

//favorite subject areas
app.post("/favoriteSubjectArea2", (req,res) => {
    console.log(req);
    let subjectAreaArray = req.body;
    console.log(subjectAreaArray);
    for(let subjectArea of subjectAreaArray) {
        FavoriteSubjectArea.create(subjectArea)
            .then( () => console.log("Succesfully added favorie subject area"))
            .catch((err) => {
                console.warn(err)
                res.status(500).send('some error')
            });
    }
    res.status(201).send("Succesfully added favorite subjects area")
});


app.get('/favoriteSubjectArea', (req, res) => {
	sequelize.query("select subject_areas.*, favorite_subject_areas.id as idArea from subject_areas " +
					"inner join favorite_subject_areas on subject_areas.id = favorite_subject_areas.subjectAreaId " +
					" where favorite_subject_areas.userId = '" + req.query.userId + "';")
            .then( (subjectAreas) =>  {
                if(subjectAreas) {
                    console.log(subjectAreas[0])
                    res.status(200).json(subjectAreas[0]);
                }
                else {
                    res.status(404).send("Errors");
                }
            })  .catch( (err) => {
            console.warn(err);
            res.status(500).send("Some errors");
            })
})

app.delete("/favoriteSubjectArea/:id", (req,res) => {
    FavoriteSubjectArea.findByPk(req.params.id)
        .then( (subjectArea) => {
            if(subjectArea) {
                subjectArea.destroy();
            }
            else {
                res.status(404).send("Favorite subject area not found");
            }
        })
        .then( () => {res.status(201).send("Favorite subject area deleted") })
        .catch((err) => {
            console.warn(err);
            res.status(500).send('some error');
        })

});
//favorite subject areas

//subject areas
app.get("/subjectArea", (req,res) => {
    SubjectArea.findAll()
        .then( (subjectAreas) => {res.status(200).json(subjectAreas)})
        .catch( (err) => {
            console.warn(err);
            res.status(500).send("Some errors");
        })
});

app.get("/subjectArea/:id", (req, res) => {
    SubjectArea.findById(req.params.id)
        .then( (subjectArea) => {res.status(200).json(subjectArea)})
        .catch( (err) => {
            console.warn(err);
            res.status(500).send("Some errors");
        })
});
//subject areas


app.listen(8080);
