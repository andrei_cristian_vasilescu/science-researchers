import axios from 'axios'

const SERVER = 'http://localhost:8080'

class ProfileStore {

  constructor(ee){
    this.ee = ee
    this.profiles = null
  }

  search(query){
    axios.get(SERVER + '/searchProfile?find=' + query)
      .then((response) => {
        this.profiles = response.data
        this.ee.emit('SEARCH_PROFILE')
      })
      .catch((error) => {
        this.profiles = null
        this.ee.emit('SEARCH_PROFILE')
      })
  }
}

export default ProfileStore