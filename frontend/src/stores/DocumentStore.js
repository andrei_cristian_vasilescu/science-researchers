import axios from 'axios'

const SERVER = 'http://localhost:8080'

class DocumentStore {

  constructor(ee){
    this.ee = ee
    this.documents = null
    this.favoriteDocuments = null
    this.documentDetails = null
  }


  addOne(document){
    axios.post(SERVER + '/favoriteDocument', document)
      .then((response) => {
        console.log("Succes document")
      })
      .catch((error) => {
        console.warn(error)
      })
  }

  searchByUserId(userId){
    axios.get(SERVER + '/favoriteDocument/user/' + userId)
      .then((response) => {
        this.favoriteDocuments = response.data;
        this.ee.emit('SEARCH_FAVORITE_DOCUMENT')
        
      })
      .catch((error) => {
        this.favoriteDocuments = null;
        this.ee.emit('SEARCH_FAVORITE_DOCUMENT')
      })
  }

  searchById(documentId) {
      axios.get(SERVER + '/favoriteDocument/' + documentId)
      .then((response) => {
        this.documentDetails = response.data;
        this.ee.emit('FAVORITE_DOCUMENT_DETAILS')
        
      })
      .catch((error) => {
        this.documentDetails = null;
        this.ee.emit('FAVORITE_DOCUMENT_DETAILS')
      })
  }

  deleteFavoriteDocument(id){
    axios.delete(SERVER + '/favoriteDocument/' + id)
      .then((response) => {
        console.log("Deleted")
      })
      .catch((error) => {
        console.warn(error)
      })
  }

  search(query){
    axios.get(SERVER + '/searchDocument?find=' + query)
      .then((response) => {
        this.documents = response.data
        console.log(this.documents)
        this.ee.emit('SEARCH_DOCUMENT')
      })
      .catch((error) => {
        this.documents = null
        this.ee.emit('SEARCH_DOCUMENT')
      })
  }
}

export default DocumentStore