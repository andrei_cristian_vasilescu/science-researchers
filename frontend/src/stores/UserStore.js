import axios from 'axios'

const SERVER = 'http://localhost:8080'

class UserStore {

  constructor(ee){
    this.ee = ee
    this.user = null,
    this.message = null,
    this.isLoggedId= false;
  }

  addOne(user){
    axios.post(SERVER + '/signup', user)
      .then((response) => {
        this.message = "Succesfully sign up"
        this.ee.emit('USER_SIGN_UP')
      })
      .catch((error) => {
        console.warn(error)
        this.message = "Errors"
        this.ee.emit('USER_SIGN_UP')
      })
  }

  logIn(user){
    axios.post(SERVER + '/login', user)
      .then((response) => {
        this.user = response.data
        this.message=null
        this.isLoggedId = true;
        this.ee.emit('USER_LOG_IN')
      })
      .catch((error) => {
        this.user=null;
        this.message="Invalid username or password"
        this.isLoggedId = false
        this.ee.emit('USER_LOG_IN')
      })
  }
}

export default UserStore