import axios from 'axios'

const SERVER = 'http://localhost:8080'

class SubjectAreaStore {

  constructor(ee){
    this.ee = ee
    this.subjectAreas = []
    this.favoriteSubjectAreas = []
    
  }

  addOne(subjectArea,userId){
    axios.post(SERVER + '/favoriteSubjectArea', subjectArea)
      .then((response) => {
          this.getByUserId(userId)
      })
      .catch((error) => {
       
      })
  }

  getByUserId(userId){
    axios.get(SERVER + '/favoriteSubjectArea?userId=' + userId)
      .then((response) => {
        console.log(response.data)
        this.favoriteSubjectAreas = response.data;
        this.ee.emit('FAVORITE_SUBJECT_AREAS')
        
      })
      .catch((error) => {
        this.favoriteSubjectAreas = [];
        this.ee.emit('FAVORITE_SUBJECT_AREAS')
      })
  }

  delete(id,userId){
    axios.delete(SERVER + '/favoriteSubjectArea/' + id)
      .then((response) => {
        this.getByUserId(userId)
        console.log("Deleted")
      })
      .catch((error) => {
        console.warn(error)
      })
  }

  getAll(){
    axios.get(SERVER + '/subjectArea')
      .then((response) => {
        this.subjectAreas = response.data;
        this.ee.emit('SUBJECT_AREAS')
        
      })
      .catch((error) => {
        this.favoriteDocuments = [];
        this.ee.emit('SUBJECT_AREAS')
      })
  }

}

export default SubjectAreaStore