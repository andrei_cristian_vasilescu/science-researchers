import React, { Component } from 'react'
import {EventEmitter} from 'fbemitter'
import DocumentStore from '../stores/DocumentStore';
import DocumentList from './DocumentList';
import FavoriteDocumentList from './FavoriteDocumentList';

const ee = new EventEmitter()
const documentStore = new DocumentStore(ee)

class FavoriteDocuments extends Component {
  constructor(props){
    super(props)
    this.state = {
      userId: this.props.userId,
      favoriteDocuments: null,
      isViewSelectetd: false
    };
    this.searchFavoriteDocuments = this.searchFavoriteDocuments.bind(this);
    this.handleChange = (event) => {
      this.setState({
        [event.target.name] : event.target.value
      })
    }
  }

  searchFavoriteDocuments() {
      this.setState({
          isViewSelectetd: !this.state.isViewSelectetd
      })
      documentStore.searchByUserId(this.state.userId)
  }

  componentDidMount(){
    ee.addListener('SEARCH_FAVORITE_DOCUMENT', () => {
        this.setState({
            favoriteDocuments:documentStore.favoriteDocuments
        })
        console.log(this.state.favoriteDocuments)
    })
    documentStore.searchByUserId(this.state.userId)
  
  }

  render() {
    return (
      <div class="container">
            {this.state.favoriteDocuments?<FavoriteDocumentList documents={this.state.favoriteDocuments}/>:null}
      </div>
      
    ) 
  }
}

export default FavoriteDocuments