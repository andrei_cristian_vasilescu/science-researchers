import React, { Component } from 'react'
import {EventEmitter} from 'fbemitter'

const ee = new EventEmitter()

class Profile extends Component {
  constructor(props){
    super(props)
    this.state = {
      profile: this.props.profile,
      showDetails: false,
    };
    this._onButtonClickDetails = this._onButtonClickDetails.bind(this);
 
  }

  _onButtonClickDetails() {
      this.setState({
        showDetails: !this.state.showDetails
      })
  }

  render() {
    return (
            <tr>
                <td>{this.state.profile.first_name + " " + this.state.profile.last_name}</td>
                <td>{this.state.profile.discipline.name}</td>
                <td>{this.state.profile.academic_status}</td>
                <td>{this.state.profile.link}</td>

                <td> <button type="button" class="btn btn-primary" onClick={this._onButtonClickDetails} data-toggle="modal" data-target={"#" + this.state.profile.id}>Details</button></td>
                
                <div class="modal fade" id={this.state.profile.id} role="dialog">
                  <div class="modal-dialog">
                    <div class="modal-content">
                      <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h3 class="modal-title">{this.state.profile.display_name}</h3>
                      </div>
                      <div class="modal-body">
                      <table class="table table-striped">
                          <tbody>
                            <tr>
                              <td></td>
                              <td><img src={this.state.profile.photo.standard}/></td>
                            </tr>
                            <tr>
                              <td>Academic status</td>
                              <td>{this.state.profile.academic_status}</td>
                            </tr>
                            <tr>
                              <td>Discipline</td>
                              <td>{this.state.profile.discipline.name}</td>
                            </tr>
                            <tr>
                              <td>Link</td>
                              <td>{this.state.profile.link}</td>
                            </tr>
                            <tr>
                              <td>Folder</td>
                              <td>{this.state.profile.folder}</td>
                            </tr>
                           
                          </tbody>
                      </table>
                      </div>
                      <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                      </div>
                    </div>
                    
                  </div>
              </div>
             
                    
                   
            </tr>
            
           
    )
  }
}

export default Profile