import React, { Component } from 'react'
import {EventEmitter} from 'fbemitter'
import ProfileStore from '../stores/ProfileStore';
import DocumentList from './DocumentList';
import ProfileList from './ProfileList';

const ee = new EventEmitter()
const profileStore = new ProfileStore(ee)

class SearchProfile extends Component {
  constructor(props){
    super(props)
    this.state = {
      query: "",
      profiles: null
    };
    this.searchProfile = this.searchProfile.bind(this);

    this.handleChange = (event) => {
      this.setState({
        [event.target.name] : event.target.value
      })
    }
  }

  searchProfile() {
    this.setState({
      profiles:null
    })
    if(this.state.query.length>0) {
        profileStore.search(this.state.query)
    }
  }

  componentDidMount(){
    ee.addListener('SEARCH_PROFILE', () => {
        this.setState({
            profiles:profileStore.profiles
        })
    })
  
  }

  render() {
    return (
      <div class="container">
            <div class="form-group">
                <input type="text" class="form-control" id="query" name="query" placeholder="Enter query" onChange={this.handleChange}/>
            </div>
            <button type="button" class="btn btn-primary" onClick={this.searchProfile}>Search</button>
            {this.state.profiles ? <ProfileList profiles={this.state.profiles}/>:null}
      </div>
    ) 
  }
}

export default SearchProfile