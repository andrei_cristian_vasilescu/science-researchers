import React, { Component } from 'react'
import Profile from './Profile';

class ProfileList extends Component {
  constructor(props){
    super(props)
    this.state = {
      profiles: this.props.profiles,
    }
    this.handleChange = (event) => {
      this.setState({
        [event.target.name] : event.target.value
      })
    }
  }
  render() {
    return (
        <div class="container">
             <table class="table table-striped">
                <thead>
                <tr>
                    <th>Name</th>
                    <th>Discipline</th>
                    <th>Academic status</th>
                    <th>Link</th>
                    <th></th>
                </tr>
                </thead>
                <tbody>
                {
                    this.state.profiles.map((p) => 
                      <Profile profile={p} />
                    )
                }
                </tbody>
            </table>
        </div>
    )
  }
}

export default ProfileList