import React, { Component } from 'react'
import DocumentStore from '../stores/DocumentStore';
import {EventEmitter} from 'fbemitter'

const ee = new EventEmitter()
const documentStore = new DocumentStore(ee)

class FavoriteDocument extends Component {
  constructor(props){
    super(props)
    this.state = {
      document: this.props.document,
      documentDetails: null
    };
    this._onButtonClickDetails = this._onButtonClickDetails.bind(this);
    this._onButtonDelete = this._onButtonDelete.bind(this);
   
  }

  _onButtonClickDetails() {
      this.setState({
        documentDetails:null
      })
      documentStore.searchById(this.state.document.document_id)
  }

  _onButtonDelete() {
     documentStore.deleteFavoriteDocument(this.state.document.id)
  }

  componentDidMount() {
    ee.addListener('FAVORITE_DOCUMENT_DETAILS', () => {
      this.setState({
          documentDetails:documentStore.documentDetails
      })
      
  })
  }

  render() {
    return (
            <tr>
                <td>{this.state.document.document_id}</td>
                <td>{this.state.document.added_date}</td>
                <td> <button type="button" class="btn btn-primary" onClick={this._onButtonClickDetails} data-toggle="modal" data-target={"#" +this.state.document.document_id}>Details</button></td>
                <td> <button type="button" class="btn btn-danger" onClick={this._onButtonDelete}>Delete</button></td>


                {this.state.documentDetails ? 
                <div class="modal fade" id={this.state.document.document_id} role="dialog">
                  <div class="modal-dialog">
                    <div class="modal-content">
                      <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h3 class="modal-title">{this.state.documentDetails.title}</h3>
                      </div>
                      <div class="modal-body">
                            
                      <table class="table table-striped">
                          <tbody>
                            <tr>
                              <td>ID</td>
                              <td>{this.state.documentDetails.id}</td>
                            </tr>
                            <tr>
                              <td>Type</td>
                              <td>{this.state.documentDetails.type}</td>
                            </tr>
                            <tr>
                              <td>Year</td>
                              <td>{this.state.documentDetails.year}</td>
                            </tr>
                            <tr>
                              <td>Source</td>
                              <td>{this.state.documentDetails.source}</td>
                            </tr>
                            <tr>
                              <td>Authors</td>
                              <td>
                              {
                                  this.state.documentDetails.authors.map((a) => 
                                    a.last_name + " "
                                  )
                              }
                              </td>
                            </tr>
                            <tr>
                              <td>Link</td>
                              <td>{this.state.documentDetails.link}</td>
                            </tr>
                            <tr>
                              <td>Description</td>
                              <td>{this.state.documentDetails.abstract}</td>
                            </tr>

                          </tbody>
                      </table>
                      </div>
                      <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                      </div>
                    </div>
                    
                  </div>
              </div>
              :null}

            </tr>
            
           
    )
  }
}

export default FavoriteDocument