import React, { Component } from 'react'
import FavoriteDocument from "./FavoriteDocument";

class FavoriteDocumentList extends Component {
    constructor(props){
      super(props)
      this.state = {
        userId: this.props.userId,
        documents: this.props.documents,
      }
      this.handleChange = (event) => {
        this.setState({
          [event.target.name] : event.target.value
        })
      }
    }
    render() {
      return (
          <div class="container">
               <table class="table table-striped">
                  <thead>
                  <tr>
                      <th>ID</th>
                      <th>Added date</th>
                      <th></th>
                      <th></th>
                  </tr>
                  </thead>
                  <tbody>
                  {
                      this.state.documents.map((d) => 
                        <FavoriteDocument document={d} />
                      )
                  }
                  </tbody>
              </table>
          </div>
      )
    }
  }
  
  export default FavoriteDocumentList