import React, { Component } from 'react'
import {EventEmitter} from 'fbemitter'
import SubjectAreaStore from '../stores/SubjectAreaStore';

const ee = new EventEmitter()
const subjectAreasStore = new SubjectAreaStore(ee)

class FavoriteSubjectArea extends Component {
  constructor(props){
    super(props)
    this.state = {
        subjectAreas : [],
        favoriteSubjectAreas: [],
        selectedArea :null,
        userId: this.props.userId,
    };
    this.handleSelect = this.handleSelect.bind(this);
    this._onButtonClickAdd = this._onButtonClickAdd.bind(this);
    this._onButtonClickDelete = this._onButtonClickDelete.bind(this);
   
  }

  componentDidMount(){
    ee.addListener('SUBJECT_AREAS', () => {
        this.setState({
            subjectAreas:subjectAreasStore.subjectAreas
        })
    })
    ee.addListener('FAVORITE_SUBJECT_AREAS', () => {
      this.setState({
        favoriteSubjectAreas:subjectAreasStore.favoriteSubjectAreas
      })
    })
    subjectAreasStore.getAll()
    subjectAreasStore.getByUserId(this.state.userId)
  
  }

  _onButtonClickAdd() {
    subjectAreasStore.addOne({
      userId: this.state.userId,
      subjectAreaId: this.state.selectedArea
    },this.state.userId)
  }

  _onButtonClickDelete(e) {
        subjectAreasStore.delete(e.target.id,this.state.userId)
  }

  handleSelect(e) {
    this.setState({
      selectedArea: e.target.value
    })
  }

  render() {
    return (
      <div class="container">
        <div class="container">
          <label>Select your favorite subject area</label>
            <select onChange={this.handleSelect}>
                {
                      this.state.subjectAreas.map((s) => 
                        <option value={s.id}>{s.name}</option>
                      )
                  }
            
            </select>
           
            {this.state.selectedArea ?
            <button type="button" class="btn btn-primary" onClick={this._onButtonClickAdd}>Add</button>
            :
            null}
          </div>
          <div class="container">
          <table class="table table-striped">
              <tbody>
                {
                    this.state.favoriteSubjectAreas.map((s) => 
                    <tr>
                      <td>
                        {s.name}
                        <button id={s.idArea} onClick={this._onButtonClickDelete} type="button" class="close" aria-label="Close">
                          <span id={s.idArea} aria-hidden="true">&times;</span>
                        </button>
                      </td>
                    </tr>
                    
                    )
                }
              </tbody>
            </table>
          
          </div>
      </div>
    ) 
  }
}

export default FavoriteSubjectArea