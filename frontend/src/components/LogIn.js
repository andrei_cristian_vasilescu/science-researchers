import React, { Component } from 'react'

class LogIn extends Component {
  constructor(props){
    super(props)
    this.state = {
      username: '',
      password: '',
    }
    this.handleChange = (event) => {
      this.setState({
        [event.target.name] : event.target.value
      })
    }
  }
  render() {
    return (
     
      <div class="wrapper">
      <form class="form-signin">       
        <h2 class="form-signin-heading">Please login</h2>
        <input type="text" class="form-control" id="username" name="username" placeholder="Username" required="" autofocus="" onChange={this.handleChange}/>
        <input type="password" class="form-control" id="password" id="password"  name="password" placeholder="Password" required="" onChange={this.handleChange}/>      
        
        <button class="btn btn-lg btn-primary btn-block" type="button"onClick={() => this.props.onAdd({
                        username : this.state.username,
                        password : this.state.password,
                    })}>Log In</button>   
      </form>
      </div>
    )
  }
}

export default LogIn