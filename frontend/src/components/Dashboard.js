import React, { Component } from 'react'
import Search from './Search';
import FavoriteDocuments from './FavoriteDocuments';
import FavoriteSubjectArea from './FavoriteSubjectArea';
import SearchProfile from './SearchProfile';

class Dashboard extends Component {
  constructor(props){
    super(props)
    this.state = {
      user: this.props.user,
      showSearchDocumentComponent: false,
      showMyFavoriteDocumentsComponent: false,
      showFavoriteSubjectAreasComponent: false,
      showSearchProfileComponent: false
    };
    this.showSearchComponent = this.showSearchComponent.bind(this);
    this.showMyFavoriteDocuments = this.showMyFavoriteDocuments.bind(this);
    this.showFavoriteSubjectAreas = this.showFavoriteSubjectAreas.bind(this);
    this.showSearchProfile = this.showSearchProfile.bind(this);

  }
  
  showFavoriteSubjectAreas() {
    this.setState({
      showSearchDocumentComponent: false,
      showMyFavoriteDocumentsComponent: false,
      showFavoriteSubjectAreasComponent: true,
      showSearchProfileComponent:false

    });
  }

  showSearchProfile() {
    this.setState({
      showSearchDocumentComponent: false,
      showMyFavoriteDocumentsComponent: false,
      showFavoriteSubjectAreasComponent: false,
      showSearchProfileComponent:true
    });
  }

  showSearchComponent() {
    this.setState({
      showSearchDocumentComponent: true,
      showMyFavoriteDocumentsComponent: false,
      showFavoriteSubjectAreasComponent: false,
      showSearchProfileComponent:false
    });
  }

  showMyFavoriteDocuments() {
    this.setState({
      showSearchDocumentComponent: false,
      showMyFavoriteDocumentsComponent: true,
      showFavoriteSubjectAreasComponent: false,
      showSearchProfileComponent:false

    });
  }

  render() {
      return(
            <div>
                <nav class="navbar navbar-default">
                    <div class="container-fluid">
                        <div class="navbar-header">
                            <a class="navbar-brand" > Hello { this.state.user.first_name }  {this.state.user.last_name}  </a>
                        </div>
                        <ul class="nav navbar-nav">
                            <li className={ this.state.showFavoriteSubjectAreasComponent ? "active" : ""} onClick={this.showFavoriteSubjectAreas}><a href="#">Favorite subject areas</a></li>
                            <li className={ this.state.showSearchDocumentComponent ? "active" : ""}><a href="#"  onClick={this.showSearchComponent}>Search document</a></li>
                            <li className={ this.state.showMyFavoriteDocumentsComponent ? "active" : ""} onClick={this.showMyFavoriteDocuments}><a href="#">My favorite documents</a></li>

                        </ul>
                    </div>
                </nav>
                {
                    this.state.showSearchDocumentComponent ?
                    <Search userId={this.state.user.id}/>:null
                }
                 {
                    this.state.showMyFavoriteDocumentsComponent ?
                    <FavoriteDocuments userId={this.state.user.id}/>:null
                }
                {
                    this.state.showFavoriteSubjectAreasComponent ?
                    <FavoriteSubjectArea userId={this.state.user.id}/>
                    :
                    null
                }
                {
                    this.state.showSearchProfileComponent ?
                    <SearchProfile/>
                    :
                    null
                }
            </div>
            
      )
  }
}

export default Dashboard
