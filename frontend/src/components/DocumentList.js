import React, { Component } from 'react'
import Document from './Document';

class DocumentList extends Component {
  constructor(props){
    super(props)
    this.state = {
      userId: this.props.userId,
      documents: this.props.documents,
    }
    this.handleChange = (event) => {
      this.setState({
        [event.target.name] : event.target.value
      })
    }
  }
  render() {
    return (
        <div class="container">
             <table class="table table-striped">
                <thead>
                <tr>
                    <th>Title</th>
                    <th>Year</th>
                    <th>Link</th>
                    <th></th>
                    <th></th>
                </tr>
                </thead>
                <tbody>
                {
                    this.state.documents.map((d) => 
                      <Document userId={this.state.userId} document={d} />
                    )
                }
                </tbody>
            </table>
        </div>
    )
  }
}

export default DocumentList