import React, { Component } from 'react'
import '../styles/App.css'
import SignUp from './SignUp'
import LogIn from './LogIn'
import UserStore from '../stores/UserStore'
import {EventEmitter} from 'fbemitter'
import Dashboard from './Dashboard';

const ee = new EventEmitter()
const userStore = new UserStore(ee);

function addUser(user){
    userStore.addOne(user)
}

function logIn(user){
    userStore.logIn(user);
}

class Main extends Component {

constructor(props) {
    super(props);
    this.state = {
        showLogInComponent: true,
        showSignUpComponent: false,
        user: null,
        message: null,
        isLoggedIn: false
        
    };
    this._onButtonClickLogIn = this._onButtonClickLogIn.bind(this);
    this._onButtonClickSignUp = this._onButtonClickSignUp.bind(this);
    this._onButtonClickLogOut = this._onButtonClickLogOut.bind(this);
    }

    _onButtonClickLogOut() {
        this.setState({
            showLogInComponent: true,
            showSignUpComponent: false,
            message: null,
            user: null,
            isLoggedIn:false

        });
    }

    _onButtonClickLogIn() {
        this.setState({
            showLogInComponent: true,
            showSignUpComponent: false,
            message: null
        });
    }

    _onButtonClickSignUp() {
        this.setState({
            showLogInComponent: false,
            showSignUpComponent: true,
            message: null
        });
    }

   

    componentDidMount(){
        ee.addListener('USER_LOG_IN', () => {
            this.setState({
                user: userStore.user,
                message: userStore.message,
                isLoggedIn: userStore.isLoggedId,
                showLogInComponent: false
            })
            console.log(this.state.user)
        })
        ee.addListener('USER_SIGN_UP', () => {
            this.setState({
                message: userStore.message
            })
        })
      }


  render() {
    return (
    <div>
        <div>
            <nav class="navbar navbar-inverse">
                <div class="container-fluid">
                    <ul class="nav navbar-nav navbar-right">
                    {!this.state.isLoggedIn ?
                        <li><a href="#" onClick={this._onButtonClickSignUp}><span class="glyphicon glyphicon-user"></span> Sign Up</a></li>
                        : null 
                    }

                    {!this.state.isLoggedIn ?
                        <li><a href="#" onClick={this._onButtonClickLogIn}><span class="glyphicon glyphicon-log-in"></span> Log In</a></li>
                        :null
                    }
                     {this.state.isLoggedIn ?
                    <li><a href="#" onClick={this._onButtonClickLogOut}><span class="glyphicon glyphicon-log-out"></span> Log out</a></li>
                    :null
                    }
                    </ul>
                </div>
            </nav>
        </div>

        {this.state.showLogInComponent ?
           <LogIn onAdd={logIn}/> :
           null
        }
        {this.state.showSignUpComponent ?
           <SignUp onAdd={addUser}/> :
           null
        }

        {this.state.message ?
           <div class="text-center">{this.state.message}</div> :
           null
        }

        {this.state.user ?
         <Dashboard user={this.state.user}/> :
         null
      }

    </div>

    
    )
  }
}

export default Main
