import React, { Component } from 'react'
import {EventEmitter} from 'fbemitter'
import DocumentStore from '../stores/DocumentStore';
import DocumentList from './DocumentList';

const ee = new EventEmitter()
const documentStore = new DocumentStore(ee)

class Search extends Component {
  constructor(props){
    super(props)
    this.state = {
      userId: this.props.userId,
      query: "",
      documents: null
    };
    this.searchDocument = this.searchDocument.bind(this);
    this.handleChange = (event) => {
      this.setState({
        [event.target.name] : event.target.value
      })
    }
  }

  searchDocument() {
      this.setState({
        documents:null
      })
      if(this.state.query.length>0) {
        documentStore.search(this.state.query)
      }
  }

  componentDidMount(){
    ee.addListener('SEARCH_DOCUMENT', () => {
        this.setState({
           documents:documentStore.documents
        })
    })
  
  }

  render() {
    return (
      <div class="container">
            <div class="form-group">
                <input type="text" class="form-control" id="query" name="query" placeholder="Enter query" onChange={this.handleChange}/>
            </div>
            <button type="button" class="btn btn-primary" onClick={this.searchDocument}>Search</button>
            {this.state.documents ? <DocumentList userId={this.state.userId} documents={this.state.documents}/>:null}
      </div>
    ) 
  }
}

export default Search