import React, { Component } from 'react'
import DocumentStore from '../stores/DocumentStore';
import {EventEmitter} from 'fbemitter'

const ee = new EventEmitter()
const documentStore = new DocumentStore(ee)

class Document extends Component {
  constructor(props){
    super(props)
    this.state = {
      document: this.props.document,
      showDetails: false,
      userId: this.props.userId
    };
    this._onButtonClickDetails = this._onButtonClickDetails.bind(this);
    this._onButtonAddToFavorite = this._onButtonAddToFavorite.bind(this);
   
  }

  _onButtonClickDetails() {
      this.setState({
        showDetails: !this.state.showDetails
      })
  }

  _onButtonAddToFavorite() {
      var doc = {
        document_id:this.state.document.id,
        userId:this.state.userId,
        added_date: new Date()
      }
      documentStore.addOne(doc)
  }

  render() {
    console.log(this.state.document.authors)
    return (
            <tr>
                <td>{this.state.document.title}</td>
                <td>{this.state.document.year}</td>
                <td>{this.state.document.link}</td>
              
                <td> <button type="button" class="btn btn-primary" onClick={this._onButtonClickDetails} data-toggle="modal" data-target={"#" + this.state.document.id}>Details</button></td>
                <td> <button type="button" class="btn btn-warning" onClick={this._onButtonAddToFavorite}>Add to favorite</button></td>
      
                <div class="modal fade" id={this.state.document.id} role="dialog">
                  <div class="modal-dialog">
                    <div class="modal-content">
                      <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h3 class="modal-title">{this.state.document.title}</h3>
                      </div>
                      <div class="modal-body">
                        

                      <table class="table table-striped">
                          <tbody>
                            <tr>
                              <td>ID</td>
                              <td>{this.state.document.id}</td>
                            </tr>
                            <tr>
                              <td>Type</td>
                              <td>{this.state.document.type}</td>
                            </tr>
                            <tr>
                              <td>Year</td>
                              <td>{this.state.document.year}</td>
                            </tr>
                            <tr>
                              <td>Source</td>
                              <td>{this.state.document.source}</td>
                            </tr>
                            <tr>
                              <td>Authors</td>
                              <td>
                              {
                                  this.state.document.authors &&
                                  this.state.document.authors.map((a) => 
                                    a.last_name + " " + a.fi
                                  )
                              }
                              </td>
                            </tr>
                            <tr>
                              <td>Link</td>
                              <td>{this.state.document.link}</td>
                            </tr>
                            <tr>
                              <td>Description</td>
                              <td>{this.state.document.abstract}</td>
                            </tr>

                          </tbody>
                      </table>



                      </div>
                      <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                      </div>
                    </div>
                    
                  </div>
              </div>
             
                    
                   
            </tr>
            
           
    )
  }
}

export default Document