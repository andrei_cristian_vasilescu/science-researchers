import React, { Component } from 'react'

class SignUp extends Component {
  constructor(props){
    super(props)
    this.state = {
      firstName : '',
      lastName : '',
      username: '',
      password: '',
      birthday: '',
      gender: ''
    }
    this.handleChange = (event) => {
      this.setState({
        [event.target.name] : event.target.value
      })
      console.warn(this.state)
    }
  }
  render() {
    return (
      <div class="wrapper">
          <form class="form-signin">
                <h2 class="form-signin-heading">Please sign up</h2>

                    <input type="text" class="form-control" id="username" name="username" placeholder="Username" onChange={this.handleChange}/>

                    <input type="password" class="form-control" id="password" name="password" placeholder="Password" onChange={this.handleChange}/>
               
                    <input type="text" class="form-control" id="firstName" name="firstName" placeholder="First Name" onChange={this.handleChange}/>
                
                    <input type="text" class="form-control" id="lastName" name="lastName" placeholder="Last Name" onChange={this.handleChange}/>
               
                
                    <input type="text" class="form-control" id="birthday" name="birthday" placeholder="mm/dd/yyyy" onChange={this.handleChange}/>
                
                <div class="container">
                    <label for="gender">Gender</label>
                    <div class="radio">
                        <label><input type="radio" id="gender" name="gender" value="m" onChange={this.handleChange}/>Masculin</label>
                    </div>
                    <div class="radio">
                         <label><input type="radio" id="gender" name="gender" value="f" onChange={this.handleChange}/>Feminin</label>
                    </div>
                </div>
                <button type="button" class="btn btn-lg btn-primary btn-block" onClick={() => this.props.onAdd({
                        first_name : this.state.firstName, 
                        last_name : this.state.lastName,
                        username : this.state.username,
                        password : this.state.password,
                        birthday : this.state.birthday,
                        gender : this.state.gender,
                    })}>Sign Up</button>
            </form>
      </div>
    )
  }
}

export default SignUp